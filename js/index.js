/**
 * Created by 菲菲 on 2016/7/24.
 */
$(function () {
    banner();
    fei();
});
//动态轮播
function banner() {
    //1.获取数据 ajax
    //2.判断当前是什么设备 <768px -移动端
    //3.根据设备解析数据  JSON-html 字符串拼接 模板引擎 artTemplate native underscorejs template 搭配backbone
    //4.渲染在html页面中 html()
    //5.当页面尺寸改变的时候  重新渲染  监听页面尺寸的改变
    var myData;
    var getData = function (callback) {
        if (myData) {
            callback && callback(myData);
            return false;
        }
        $.ajax({
            url: "./js/index.json",
            type: "get",
            data: {},
            dataType: "json",
            success: function (data) {
                myData = data;
                callback && callback(myData);
            }
        });
    };
    //渲染

    var render = function () {
        var width = $(window).width();
        /*判断当前是不是移动端*/
        var isMobile = false;
        if (width < 768) {//小于768px的时候认为是移动设备
            isMobile = true;
        }
        getData(function (data) {
            var templatePoint = _.template($("#template_point").html());
            var templateImage = _.template($("#template_image").html());
            var htmlPoint = templatePoint({model: data});
            var htmlImage = templateImage({model: data, isMobile: isMobile});
            $(".carousel-indicators").html(htmlPoint);
            $(".carousel-inner").html(htmlImage);
        });
    };
    render();
    $(window).on("resize", function () {
        render();
    }).trigger("resize");
    //移动端 手势滑动
    var startX = 0;
    var moveX = 0;
    var distanceX = 0;
    var isMove = false;
    $(".carousel-inner").on("touchstart", function (e) {
        startX = e.originalEvent.touches[0].clientX;
    });
    $(".carousel-inner").on("touchmove", function (e) {
        moveX = e.originalEvent.touches[0].clientX;
        distanceX = moveX - startX;
        isMove = true;
    });
    $(".carousel-inner").on("touchend", function (e) {
        if (Math.abs(distanceX) > 50 && isMove) {
            if (distanceX > 0) {
                //上一张
                console.log("prev");
                $(".carousel").carousel("prev");
            } else {
                //下一张
                console.log("next");
                $(".carousel").carousel("next");
            }
        }
    });
}
/*页面飞的小图片*/
function fei(){
    /**
     * 图片能够自由飞翔
     */
    //1.获取要操作的dom
    var timer=null;
    var left= 0,top=0;
    var winWid=$(window).innerWidth();
    var winHei=$(window).innerHeight();
    console.log(winWid);
    var timer=setInterval(function () {
            left=Math.random()*winWid;
            top=Math.random()*winHei;
        $(".fei").animate({
            top:top+"px",
            left:left+"px"
        },5000);
    },5000);
    $(".fei").mouseover(function () {
        $(".fei").css({cursor:"pointer"});
        clearInterval(timer);
    });
    $(".fei").mouseleave(function () {
        timer=setInterval(function () {
            left=Math.random()*winWid;
            top=Math.random()*winHei;
            $(".fei").animate({
                top:top+"px",
                left:left+"px"
            },5000);
        },5000);
    })
}
$(window).on('resize',function(){
    fei();
}).trigger('resize');